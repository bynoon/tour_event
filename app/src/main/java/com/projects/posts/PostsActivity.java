package com.projects.posts;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.application.GlobalApplication;
import com.apps.eventfinder.MainActivity;
import com.apps.eventfinder.R;
import com.config.Config;
import com.db.Queries;
import com.libraries.adapters.MGRecyclerAdapter;
import com.libraries.asynctask.MGAsyncTask;
import com.libraries.asynctask.MGAsyncTaskNoDialog;
import com.libraries.dataparser.DataParser;
import com.libraries.helpers.DateTimeHelper;
import com.libraries.usersession.UserAccessSession;
import com.libraries.usersession.UserSession;
import com.libraries.utilities.MGUtilities;
import com.models.DataResponse;
import com.models.Event;
import com.models.Post;
import com.models.Status;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

public class PostsActivity extends AppCompatActivity {


    private ArrayList<Post> arrayData;
    private Queries q;
    MGAsyncTaskNoDialog task;
    Event event;

    SwipeRefreshLayout swipeRefresh;
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;

    int reviewCount;
    int requestCount;
    int returnCount;
    int totalRowCount;
    int lastMaxCount;
    UserSession userSession;
    MGAsyncTask task1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        setContentView(R.layout.fragment_list_swipe);
        setTitle(R.string.posts);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        q = GlobalApplication.getQueriesInstance(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeRefresh.setClickable(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            swipeRefresh.setProgressViewOffset(false, 0,100);
        }

        swipeRefresh.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        reviewCount = 0;
        lastMaxCount = Config.MAX_POSTS_COUNT_PER_LISTING;
        arrayData = new ArrayList<Post>();

        event = (Event) this.getIntent().getSerializableExtra("event");
        userSession = UserAccessSession.getInstance(this).getUserSession();
        getData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void showRefresh(boolean show) {
        swipeRefresh.setRefreshing(show);
        swipeRefresh.setEnabled(show);
    }

    public void getData() {
        showRefresh(true);
        task = new MGAsyncTaskNoDialog(this);
        task.setMGAsyncTaskListener(new MGAsyncTaskNoDialog.OnMGAsyncTaskListenerNoDialog() {

            @Override
            public void onAsyncTaskProgressUpdate(MGAsyncTaskNoDialog asyncTask) { }

            @Override
            public void onAsyncTaskPreExecute(MGAsyncTaskNoDialog asyncTask) {

            }

            @Override
            public void onAsyncTaskPostExecute(MGAsyncTaskNoDialog asyncTask) {
                // TODO Auto-generated method stub
                showList();
            }

            @Override
            public void onAsyncTaskDoInBackground(MGAsyncTaskNoDialog asyncTask) {
                // TODO Auto-generated method stub
                if(MGUtilities.hasConnection(PostsActivity.this)) {
                    try {
                        String strUrl = String.format("%s?api_key=%s&event_id=%s&min_count=%s&max_count=%s",
                                Config.GET_POSTS_JSON_URL,
                                Config.API_KEY,
                                String.valueOf(event.getEvent_id()),
                                String.valueOf(reviewCount),
                                String.valueOf(lastMaxCount));

                        Log.d("URL", strUrl);
                        DataResponse data = DataParser.getJSONFromUrlWithPostRequest(strUrl, null);
                        if (data == null)
                            return;

                        returnCount = data.getResult_count();
                        totalRowCount = data.getTotal_no_of_rows();
                        requestCount = data.getMin_count();

                        if (data.getPosts() != null && data.getPosts().size() > 0) {
                            for (Post post : data.getPosts()) {
                                arrayData.add(post);
                            }

                            if((requestCount + returnCount) < totalRowCount) {
                                Post post = new Post();
                                post.setPost(MGUtilities.getStringFromResource(PostsActivity.this, R.string.load_more));
                                arrayData.add(post);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        task.execute();
    }

    private void showList() {
        showRefresh(false);

        if(arrayData == null || arrayData.size() == 0) {
            MGUtilities.showNotifier(this, MainActivity.offsetY);
            return;
        }

        updateList();
    }

    private void updateList() {
        MGRecyclerAdapter adapter = new MGRecyclerAdapter(arrayData.size(), R.layout.post_entry);
        adapter.setOnMGRecyclerAdapterListener(new MGRecyclerAdapter.OnMGRecyclerAdapterListener() {

            @Override
            public void onMGRecyclerAdapterCreated(MGRecyclerAdapter adapter, MGRecyclerAdapter.ViewHolder v, int position) {
                final Post post = arrayData.get(position);
                FrameLayout frameLoadMore = (FrameLayout) v.view.findViewById(R.id.frameLoadMore);
                final LinearLayout linearPost = (LinearLayout) v.view.findViewById(R.id.linearPost);
                frameLoadMore.setVisibility(View.GONE);
                linearPost.setVisibility(View.GONE);

                if(post.getFull_name() != null && !post.getFull_name().isEmpty()) {
                    TextView tvDate = (TextView) v.view.findViewById(R.id.tvDate);
                    TextView tvTitle = (TextView) v.view.findViewById(R.id.tvTitle);
                    TextView tvSubtitle = (TextView) v.view.findViewById(R.id.tvSubtitle);
                    ImageView imgViewThumb = (ImageView) v.view.findViewById(R.id.imgViewThumb);

                    tvTitle.setText(Html.fromHtml(post.getFull_name()));

                    Spanned postStr = Html.fromHtml(post.getPost());
                    postStr = Html.fromHtml(postStr.toString());
                    tvSubtitle.setText(postStr);

                    String dateStr = DateTimeHelper.formateDateFromStringUTC("yyyy-MM-dd hh:mm:ss", "dd EEE hh:mm a", post.getGmt_date_added());
                    tvDate.setText(dateStr);

                    GlobalApplication.getImageLoaderInstance(PostsActivity.this).displayImage(
                            post.getThumb_url(),
                            imgViewThumb,
                            GlobalApplication.getDisplayImageOptionsThumbInstance());

                    linearPost.setVisibility(View.VISIBLE);

                    ImageView imgDelete = (ImageView) v.view.findViewById(R.id.imgDelete);
                    imgDelete.setVisibility(View.GONE);
                    if(Config.ALLOW_COMMENT_DELETION) {
                        if(userSession != null && post.getPost_id() > 0 && post.getUser_id() ==  userSession.getUser_id()) {
                            imgDelete.setVisibility(View.VISIBLE);
                            linearPost.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    showAlertDialogDeleteReview(post);
                                }
                            });
                        }
                    }
                }
                else {
                    frameLoadMore.setVisibility(View.VISIBLE);
                    frameLoadMore.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            reviewCount += Config.MAX_POSTS_COUNT_PER_LISTING;
                            arrayData.remove(arrayData.size() - 1);
                            getData();
                        }
                    });
                }
            }
        });
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.menuNewPost:
                Intent i = new Intent(this, NewPostActivity.class);
                i.putExtra("event", event);
                i.putExtra("maxCount", lastMaxCount);
                startActivityForResult(i, Config.RESULT_CODE_NEW_POST);
                return true;
            default:
                finish();
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_posts, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(android.view.Menu menu) {
        // if nav drawer is opened, hide the action items
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(task != null)
            task.cancel(true);

        if(task1 != null)
            task1.cancel(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Config.RESULT_CODE_NEW_POST && resultCode == AppCompatActivity.RESULT_OK) {
            reviewCount = 0;
            lastMaxCount = Config.MAX_POSTS_COUNT_PER_LISTING;
            arrayData.clear();
            updateList();
            getData();
        }

    }

    private void showAlertDialogDeleteReview(final Post post) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(this.getResources().getString(R.string.alert_delete_review_title));
        alert.setMessage(this.getResources().getString(R.string.alert_delete_review_title_details));
        alert.setPositiveButton(this.getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        deleteReview(post);
                    }
                });
        alert.setNegativeButton(this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        alert.create();
        alert.show();
    }

    public void deleteReview(final Post post) {
        if(!MGUtilities.hasConnection(this)) {
            MGUtilities.showAlertView(
                    this,
                    R.string.network_error,
                    R.string.no_network_connection);
            showRefresh(false);
            return;
        }

        final UserSession userSession = UserAccessSession.getInstance(this).getUserSession();
        task1 = new MGAsyncTask(this);
        task1.setMGAsyncTaskListener(new MGAsyncTask.OnMGAsyncTaskListener() {

            DataResponse response;

            @Override
            public void onAsyncTaskProgressUpdate(MGAsyncTask asyncTask) { }

            @Override
            public void onAsyncTaskPreExecute(MGAsyncTask asyncTask) { }

            @Override
            public void onAsyncTaskPostExecute(MGAsyncTask asyncTask) {
                // TODO Auto-generated method stub
                checkResponseDelete(response);
            }

            @Override
            public void onAsyncTaskDoInBackground(MGAsyncTask asyncTask) {
                // TODO Auto-generated method stub
                ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("post_id", String.valueOf(post.getPost_id()) ) );
                params.add(new BasicNameValuePair("user_id", String.valueOf(userSession.getUser_id()) ));
                params.add(new BasicNameValuePair("login_hash", userSession.getLogin_hash() ));
                response = DataParser.getJSONFromUrlWithPostRequest(Config.DELETE_POST_URL, params);
            }
        });
        task1.execute();
    }

    private void checkResponseDelete(DataResponse response) {
        if(response != null && response.getStatus() != null) {
            Status status = response.getStatus();
            if(status.getStatus_code() == -1) {
                reviewCount = 0;
                lastMaxCount = Config.MAX_POSTS_COUNT_PER_LISTING;
                arrayData.clear();
                updateList();
                getData();
            }
            else {
                MGUtilities.showAlertView(this, R.string.network_error, status.getStatus_text());
            }
        }
        else {
            MGUtilities.showAlertView(this, R.string.network_error, R.string.login_error_undetermined);
        }
    }
}
