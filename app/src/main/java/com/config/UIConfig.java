package com.config;

import com.libraries.consent.Consent;
import com.apps.eventfinder.R;


public class UIConfig {

	public static int SLIDER_PLACEHOLDER = R.mipmap.bg_image_placeholder;

    public final static int IMAGE_PLACEHOLDER_PROFILE_THUMB = R.mipmap.bg_image_thumb_placeholder;

    public final static int IMAGE_PLACEHOLDER = R.mipmap.bg_image_placeholder;

    public static int THEME_BLACK_COLOR = R.color.colorPrimary;

    public static int BORDER_WIDTH = R.dimen.border_store_list;

    public final static int MAP_PIN_CUSTOM_LOCATION = R.mipmap.map_pin;
//Declaration and initialization
    public static final Integer[] ARRAY_WT_BG = { R.mipmap.bg1, R.mipmap.bg2, R.mipmap.bg3, R.mipmap.bg4};
    public static final Integer[] ARRAY_WT_TITLE = { R.string.wt_title_1, R.string.wt_title_2, R.string.wt_title_3, R.string.wt_title_4 };
    public static final Integer[] ARRAY_WT_DESC = { R.string.wt_desc_1, R.string.wt_desc_2, R.string.wt_desc_3, R.string.wt_desc_4 };

    public static final Integer[] ARRAY_WT_THUMB = { R.mipmap.img_walkthrough1, R.mipmap.img_walkthrough2, R.mipmap.img_walkthrough3, R.mipmap.img_walkthrough4};

    public final static Consent[] CONSENT_SCREENS = {
            new Consent(
                    "AGE_16",
                    R.string.age_consent_title,
                    R.string.age_consent_category,
                    R.string.age_consent_what,
                    R.string.age_consent_why_needed,
                    R.string.age_consent_more_information,
                    "https://gdpr-info.eu/art-8-gdpr/"),

            new Consent(
                    "BASIC_APP",
                    R.string.basic_app_consent_title,
                    R.string.basic_app_consent_category,
                    R.string.basic_app_consent_what,
                    R.string.basic_app_consent_why_needed,
                    R.string.basic_app_consent_more_information,
                    "http://example.com/gdpr"),

            new Consent(
                    "ADS",
                    R.string.ads_consent_title,
                    R.string.ads_consent_category,
                    R.string.ads_consent_what,
                    R.string.ads_consent_why_needed,
                    R.string.ads_consent_more_information,
                    "https://firebase.google.com/support/privacy")
    };
//create an array to hold a few countries in stead of consuming a whole api
    public final static String[] GDPR_COUNTRY = {
            "Austria",
            "Belgium",
            "Bulgaria",
            "Croatia",
            "Republic of Cyprus",
            "Czech Republic",
            "Denmark",
            "Estonia",
            "Finland",
            "France",
            "Germany",
            "Greece",
            "Hungary",
            "Ireland",
            "Italy",
            "Latvia",
            "Lithuania",
            "Luxembourg",
            "Malta",
            "Netherlands",
            "Poland",
            "Portugal",
            "Romania",
            "Slovakia",
            "Slovenia",
            "Spain",
            "Sweden",
            "United Kingdom"
    };
}
