package com.models;

import java.util.HashMap;
import java.util.Map;

public class Chat {
    private String chartWith;
    private Map<String,Message> messages =new HashMap<>();
    private String chartId;
    private String chartWithName;

    public Map<String, Message> getMessages() {
        return messages;
    }

    public void setMessages(Map<String, Message> messages) {
        this.messages = messages;
    }

    public String getChartWith() {
        return chartWith;
    }

    public void setChartWith(String chartWith) {
        this.chartWith = chartWith;
    }

    public String getChartId() {
        return chartId;
    }

    public void setChartId(String chartId) {
        this.chartId = chartId;
    }

    public String getChartWithName() {
        return chartWithName;
    }

    public void setChartWithName(String chartWithName) {
        this.chartWithName = chartWithName;
    }
}
