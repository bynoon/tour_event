package com.apps.classic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.apps.eventfinder.R;
import java.util.List;

public class StudentAdapter extends ArrayAdapter<Student> {

    //required variables
    Context context;
    List<Student> arraylistStudent;

    //create a constructor matching super i.e supper constructor
    public StudentAdapter(@NonNull Context context, List<Student> arraylistStudent) {
        super(context, R.layout.custom_list_item, arraylistStudent);

        this.context = context;
        this.arraylistStudent = arraylistStudent;
    }

    //override getview method
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //create a view to inflate
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_list_item, null, true);
        //Initialize views
        TextView textView_id = view.findViewById(R.id.student_id);
        TextView textView_name = view.findViewById(R.id.student_name);

        //set the adapater values to variable
        textView_id.setText(arraylistStudent.get(position).getId());
        textView_name.setText(arraylistStudent.get(position).getName());

        return view;
    }
}
