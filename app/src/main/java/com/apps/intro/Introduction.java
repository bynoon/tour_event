package com.apps.intro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.apps.eventfinder.MainActivity;
import com.apps.eventfinder.R;

import java.util.Objects;

public class Introduction extends AppCompatActivity {

    //variables declaration
    private Button btnForTour, btnForVirtualTour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);

        //change the action bar
        Objects.requireNonNull(getSupportActionBar()).setTitle("University of HULL");

        //variables initialization
        btnForTour = (Button)findViewById(R.id.btn_herefortour);
        btnForVirtualTour = (Button) findViewById(R.id.btn_hereforvirtualtour);

        //set listeners to buttons
        btnForTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(Introduction.this, "Here for tour", Toast.LENGTH_SHORT).show();
                Intent main_intent = new Intent(Introduction.this, MainActivity.class);
                startActivity(main_intent);
            }
        });
        btnForVirtualTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Introduction.this, "Here for virtual tour", Toast.LENGTH_SHORT).show();
            }
        });
    }
}