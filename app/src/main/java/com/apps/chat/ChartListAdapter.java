package com.apps.chat;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.eventfinder.R;
import com.models.Chat;

import java.util.List;

public class ChartListAdapter extends RecyclerView.Adapter<ChartListViewHolder> {
    List<Chat> chatList;
    Context context;
    public ChartListAdapter(List<Chat> chatList){
        this.chatList=chatList;
    }

    @NonNull
    @Override
    public ChartListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context=parent.getContext();
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.chart_list_item,parent,false);
        return new ChartListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChartListViewHolder holder, int position) {
       final Chat chat=chatList.get(position);
       holder.chartWithName.setText(chat.getChartWithName());

       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent=new Intent(context,ChartActivity.class);
               intent.putExtra("chartId",chat.getChartId());
               context.startActivity(intent);
           }
       });


    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }
}
