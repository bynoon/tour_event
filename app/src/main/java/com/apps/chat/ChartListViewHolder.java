package com.apps.chat;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.eventfinder.R;

public class ChartListViewHolder extends RecyclerView.ViewHolder {
    public TextView chartWithName;
    public View itemView;

    public ChartListViewHolder(@NonNull View itemView) {
        super(itemView);
        chartWithName=itemView.findViewById(R.id.chartWithName);
        this.itemView=itemView;
    }
}
