package com.apps.chat;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apps.eventfinder.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.models.Chat;

import java.util.ArrayList;
import java.util.List;

public class ChartsListActivity extends AppCompatActivity {


    FirebaseDatabase firebaseDatabase;
    DatabaseReference myChatsReference, mrs;

    FirebaseRecyclerAdapter<Chat,ChartListViewHolder> firebaseRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charts_list);

        firebaseDatabase=FirebaseDatabase.getInstance();
        myChatsReference=firebaseDatabase.getReference().child("users").child("userId1").child("chats");

        final RecyclerView chartsList=findViewById(R.id.chartList);
        chartsList.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false));
        chartsList.setItemAnimator(new DefaultItemAnimator());




        myChatsReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<Chat> chatList=new ArrayList<>();
                Iterable<DataSnapshot> snapshots=snapshot.getChildren();

                for(DataSnapshot snapshot1: snapshots){
                    Chat chat=snapshot1.getValue(Chat.class);
                    chat.setChartId(snapshot1.getKey());
                    chatList.add(chat);
                   // System.out.println("Chart With:::::::::::::::::"+chat.getChartWith());
                }

                ChartListAdapter chartListAdapter=new ChartListAdapter(chatList);
                chartsList.setAdapter(chartListAdapter);

                for (int i=0; i<chatList.size();i++){
                    chartListAdapter.notifyDataSetChanged();
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }
}