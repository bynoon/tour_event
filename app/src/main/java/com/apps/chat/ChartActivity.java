package com.apps.chat;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationManagerCompat;

import com.apps.eventfinder.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.models.Message;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ChartActivity extends AppCompatActivity {

    LinearLayout layout;
    RelativeLayout layout_2;
    ImageView sendButton;
    EditText messageArea;
    ScrollView scrollView;

    boolean isInitialOpen=true;
    List<Message> messageList=new ArrayList<>();

    String chartId;

    //firebase References

    FirebaseDatabase firebaseDatabase;
    DatabaseReference senderReference,receiverReference;


    private NotificationManagerCompat notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        sendButton=findViewById(R.id.sendButton);
        messageArea=findViewById(R.id.messageArea);
        scrollView=findViewById(R.id.scrollView1);
        layout=findViewById(R.id.layout1);

        Intent intent=getIntent();
        chartId=intent.getStringExtra("chartId");

       firebaseDatabase=FirebaseDatabase.getInstance();

       //userId1 is user node -- replace it with a variable with the users Id
        //same for chartId1 -- chartId2 -- userId2
       senderReference=firebaseDatabase.getReference("users").child("userId1").child("chats");
       receiverReference=firebaseDatabase.getReference("users").child("userId2").child("chats");


        //Listener for incoming and outgoing messages
        final ValueEventListener valueEventListener=new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                messageList.clear();
                Iterable<DataSnapshot> snapshots=snapshot.getChildren();
                for(DataSnapshot snapshot1: snapshots){
                    com.models.Message message=snapshot1.getValue(com.models.Message.class);
                    messageList.add(message);

                }
                layout.removeAllViews();
                if (isInitialOpen){
                        for (Message message: messageList){
                        int type=message.getMessageChannel().equals("in")?1:2;
                        addMessageBox(message.getMessageString(),type);
                    }
                }else {
                    //null p exception
                    Message message=snapshot.getValue(Message.class);
                    int type=message.getMessageChannel().equals("in")?1:2;
                    addMessageBox(message.getMessageString(),type);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        if (!(chartId==null)){
            senderReference.child(chartId).child("messages").addValueEventListener(valueEventListener);
        }

       sendButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if (chartId==null){
               chartId= senderReference.push().getKey();
               senderReference.child(chartId).child("chartWith").setValue("userId2");
               receiverReference.child(chartId).child("chartWith").setValue("userId1");
               senderReference.child(chartId).child("messages").addValueEventListener(valueEventListener);
               }

               if (!(messageArea.getText().toString().equals(""))){
                   String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                   Message message=new Message();
                   message.setMessageChannel("out");
                   message.setMessageString(messageArea.getText().toString());
                   message.setMessageTimeStamp(currentTime);
                   sendMessage(message);
               }

               messageArea.setText("");
           }
       });


    }


    public void addMessageBox(String message, int type) {
        TextView textView = new TextView(ChartActivity.this);
        textView.setText(message);

        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp2.weight = 1.0f;

        if (type == 1) {
            lp2.gravity = Gravity.LEFT;
            textView.setBackgroundResource(R.drawable.bubble_in);
        } else {
            lp2.gravity = Gravity.RIGHT;
            textView.setBackgroundResource(R.drawable.bubble_out);
        }
        textView.setLayoutParams(lp2);
        layout.addView(textView);
        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        },400);
    }


    private void sendMessage(Message message){
            //add message to senders database
            DatabaseReference databaseReference=senderReference.child(chartId).child("messages");
            String messageId=databaseReference.push().getKey();
            message.setMessageId(messageId);
            addMessage(message,databaseReference);

            //add message to receivers database
            DatabaseReference databaseReference1=receiverReference.child(chartId).child("messages");
            message.setMessageChannel("in");
            addMessage(message,databaseReference1);

    }

    private void addMessage(Message message, DatabaseReference databaseReference){
        databaseReference.child(message.getMessageId()).setValue(message);
    }
}