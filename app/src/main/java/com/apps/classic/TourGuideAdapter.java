package com.apps.classic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.apps.eventfinder.R;

import java.util.List;

public class TourGuideAdapter extends ArrayAdapter<TourGuide> {

    //variable required
    Context context;
    List<TourGuide> arrayListTourGuide;

    //generate super constructor
    public TourGuideAdapter(@NonNull Context context, List<TourGuide> arrayListTourGuide) {
        super(context, R.layout.custom_list_item, arrayListTourGuide);

        this.context = context;
        this.arrayListTourGuide = arrayListTourGuide;
    }

    //override getView()
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //create a view to inflate
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_list_itemtour, null, true);
        //Initialize views
        TextView textView_tourguide_id = view.findViewById(R.id.tourguide_id);
        TextView textView_touruide_name = view.findViewById(R.id.tourguide_name);
        TextView textView_tourguide_email = view.findViewById(R.id.tourguide_email);

        //set the adapater values to variable
       textView_tourguide_id.setText(arrayListTourGuide.get(position).getId());
       textView_touruide_name.setText(arrayListTourGuide.get(position).getName());
        textView_tourguide_email.setText(arrayListTourGuide.get(position).getEmail());

        return view;
    }
}
