package com.apps.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apps.chat.ChartsListActivity;
import com.apps.eventfinder.R;

import java.util.HashMap;
import java.util.Map;

public class UserRegLog extends AppCompatActivity {

    //Variables initialization
    private static final String URL_REGISTER = "https://www.opendaytourguide.com/api/signup.php";
    RequestQueue requestQueue;
    private EditText c_password;
    private EditText email;
    private TextView have_account;
    private ProgressBar loading;
    private Button login;
    private EditText name;
    private EditText password;
    private Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_reg_log);

        /*Attach to main Thread*/
        requestQueue = Volley.newRequestQueue(this);

        /*Initialization of Views*/
        register = findViewById(R.id.register);
        have_account = findViewById(R.id.have_account);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        name = findViewById(R.id.name);
        c_password = findViewById(R.id.c_password);

        //Perform register Button actions
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
               /* Intent dashboard = new Intent(MainActivity.this, Homepage.class);
                startActivity(dashboard);*/
            }
        });
        //Launch login
        have_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(UserRegLog.this, ChartsListActivity.class);
                startActivity(login);
            }
        });
    }

    //collect data and trim unnecessary spaces and store the values in strings
    private void registerUser() {
        final String user_name = this.name.getText().toString().trim();
        final String user_email = this.email.getText().toString().trim();
        final String user_password = this.password.getText().toString().trim();

        createSignup(user_name, user_email, user_password);
    }

    //createSignUp is a method that takes username, email and password to the backend database
    //via the server.
    private void createSignup(final String user_name, final String user_email, final String user_password) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                Intent login = new Intent(UserRegLog.this, UsersLogin.class);
                startActivity(login);
            }
            //Function to display any error on the request
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            //Store the values in a map before posting them in a database
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> parms = new HashMap<String, String>();
                parms.put("name", user_name);
                parms.put("email", user_email);
                parms.put("password", user_password);

                return parms;
            }
        };
        //add the string request in a Queue
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }
}


